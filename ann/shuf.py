import numpy
import h5py
import os


for i in range(20, 50):
    print i
    f = h5py.File('../ann_data/{}.h5'.format(i), 'r')
    feature = f['/FEATURE'].value
    label = f['/LABEL'].value
    f.close()
    os.remove('../ann_data/{}.h5'.format(i))
    order = range(feature.shape[0])
    numpy.random.shuffle(order)
    feature = feature[order, :]
    label = label[order]

    f = h5py.File('../ann_data/{}.h5'.format(i), 'w')
    dataDtype = feature.dtype
    dataDset = f.create_dataset('/FEATURE', feature.shape, dataDtype)
    dataDset[:] = feature
    dataDtype = label.dtype
    dataDset = f.create_dataset('/LABEL', label.shape, dataDtype)
    dataDset[:] = label
    f.close()

