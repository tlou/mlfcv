import sys
import numpy
import h5py

data_path = '../ann_data/'
raw_path = '../raw_data/npy/'

places = dict()
f = open('../ranger_scripts/places_list', 'r')
for i in range(205):
    string = f.readline()
    name = string[ :string.find(' ')]
    places [name] = i
f.close()

#validation_feature = numpy.zeros([20500, 4096], dtype = numpy.float)
#test_feature = numpy.zeros([20500, 4096], dtype = numpy.float)
#validation_label = numpy.zeros([20500], dtype = numpy.int)
#test_label = numpy.zeros([20500], dtype = numpy.int)

#for i in places:
#    print places[i], i
#    feature = numpy.load(raw_path + '{}.npy'.format(i))
#    validation_feature[(places[i] * 100):(places[i] * 100 + 100), :] = feature[:100, :]
#    test_feature[(places[i] * 100):(places[i] * 100 + 100), :] = feature[100:200, :]
#    validation_label[(places[i] * 100):(places[i] * 100 + 100)] = places[i]
#    test_label[(places[i] * 100):(places[i] * 100 + 100)] = places[i]

#f = h5py.File(data_path + 'validation.h5', 'w')
#dataDtype = validation_feature.dtype
#dataDset = f.create_dataset('/FEATURE', validation_feature.shape, dataDtype)
#dataDset[:] = validation_feature
#dataDtype = validation_label.dtype
#dataDset = f.create_dataset('/LABEL', validation_label.shape, dataDtype)
#dataDset[:] = validation_label
#f.close()

#f = h5py.File(data_path + 'test.h5', 'w')
#dataDtype = test_feature.dtype
#dataDset = f.create_dataset('/FEATURE', test_feature.shape, dataDtype)
#dataDset[:] = test_feature
#dataDtype = test_label.dtype
#dataDset = f.create_dataset('/LABEL', test_label.shape, dataDtype)
#dataDset[:] = test_label
#f.close()

num_set = 50

#prototype_feature = numpy.zeros([48527, 4096], dtype = numpy.float)
#prototype_label = numpy.zeros([48527], dtype = numpy.int)
#for set_index in range(20, 50):
#    f = h5py.File(data_path + '{}.h5'.format(set_index), 'w')
#    dataDtype = prototype_feature.dtype
#    dataDset = f.create_dataset('/FEATURE', prototype_feature.shape, dataDtype)
#    dataDset[:] = prototype_feature
#    dataDtype = prototype_label.dtype
#    dataDset = f.create_dataset('/LABEL', prototype_label.shape, dataDtype)
#    dataDset[:] = prototype_label
#    f.close()

offset_npy = 0
for place in places:
    feature = numpy.load(raw_path + '{}.npy'.format(place))
    num_sample = (feature.shape[0] - 200) / num_set
    for set_index in range(45, 50):
        offset = 200 + num_sample * set_index
        f = h5py.File(data_path + '{}.h5'.format(set_index))
        f['/FEATURE'][offset_npy:(offset_npy + num_sample), :] = feature[offset:(offset + num_sample), :]
        f['/LABEL'][offset_npy:(offset_npy + num_sample)] = places[place]
        f.close()

    offset_npy += num_sample


#for set_index in range(20):
#    print set_index
#    set_feature = numpy.zeros([0, 4096], dtype = numpy.float)
#    set_label = numpy.zeros([0], dtype = numpy.int)
#    for place in places:
#        print '\t', places[place], place
#        feature = numpy.load(raw_path + '{}.npy'.format(place))
#        num_sample = (feature.shape[0] - 200) / num_set
#        offset = 200 + num_sample * set_index
#        set_feature = numpy.concatenate([set_feature, feature[offset:(offset + num_sample), :]], axis = 0)
#        set_label = numpy.concatenate([set_label, places[place] * numpy.ones([num_sample], dtype = numpy.int)], axis = 0)
#
#    f = h5py.File(data_path + '{}.h5'.format(set_index), 'w')
#    dataDtype = set_feature.dtype
#    dataDset = f.create_dataset('/FEATURE', set_feature.shape, dataDtype)
#    dataDset[:] = set_feature
#    dataDtype = set_label.dtype
#    dataDset = f.create_dataset('/LABEL', set_label.shape, dataDtype)
#    dataDset[:] = set_label
#    f.close()
