#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "cblas.h"
#include "hdf5.h"

#define INIT_RANGE 0.06
#define LAMBDA 0.0001
#define STEP_SIZE -0.001

#define REPORT_FILE "report_50_1024_slow_backup"

const int size_data = 50;

void sigmoid(double *dest, double *source, int num) {
	int i;
	for(i = 0; i < num; i++) {
		dest[i] = 1.0 / (exp(-source[i]) + 1.0);
	}
	return;
}

void diff_sigmoid(double *dest, double *source, int num) {
	int i;
	double tmp;
	for(i = 0; i < num; i++) {
		tmp = exp(-source[i]) + 1.0;
		dest[i] = (tmp - 1.0) / (tmp * tmp);
	}
	return;
}

void dot_product(double *dest, double *source, int num) {
	int i;
	for(i = 0; i < num; i++) {
		dest[i] *= source[i];
	}
	return;
}

void forward_propagate(double *data,
		double *input_weight, double *output_weight, double **weight,
		double *input_delta, double *output_delta, double **delta,
		double *a_final, double **a, double **theta,
		int num_feature, int num_class, int num_layer, int num_node) {

	int i;
	/*feature to 1. layer*/
	cblas_dgemv(CblasRowMajor, CblasTrans, (num_feature + 1), num_node,
			1.0, input_weight, num_node, data, 1, 0.0, theta[0], 1);
	sigmoid(a[0], theta[0], num_node);

	/*1. layer to last layer*/
	for(i = 1; i < num_layer; i++) {
		cblas_dgemv(CblasRowMajor, CblasTrans, (num_node + 1), num_node,
				1.0, weight[i - 1], num_node, a[i - 1], 1, 0.0, theta[i], 1);
		sigmoid(a[i], theta[i], num_node);
	}

	/*last layer to output(a_final)*/
	cblas_dgemv(CblasRowMajor, CblasTrans, (num_node + 1), num_class,
			1.0, output_weight, num_class, a[num_layer - 1], 1, 0.0, a_final, 1);
	sigmoid(a_final, a_final, num_class);

	return;
}

void backward_propagate(double *data, int result,
		double *input_weight, double *output_weight, double **weight,
		double *input_delta, double *output_delta, double **delta,
		double *a_final, double **a, double **theta, double *tmp,
		int num_feature, int num_class, int num_layer, int num_node) {

	int i;
	int m, n;
	double *A, *x, *y;

	/*hidden to out*/
	a_final[result] -= 1.0;
	cblas_dscal(num_class * num_node, 0.0, output_delta, 1);
	cblas_dger(CblasRowMajor, num_node, num_class, 1.0,
			a[num_layer - 1], 1, a_final, 1, output_delta, num_class);
	//cblas_daxpy(num_class * num_node, LAMBDA, output_weight, 1, output_delta, 1);
	cblas_daxpy(num_class * num_node, STEP_SIZE, output_delta, 1, output_weight, 1);
	cblas_daxpy(num_class, STEP_SIZE, a_final, 1, &output_weight[num_class * num_node], 1);

	/*hidden layers*/
	m = num_node;
	n = num_class;
	x = a_final;
	y = tmp;
	A = output_weight;
	for(i = num_layer - 1; i > 0 ; i--) {
		cblas_dgemv(CblasRowMajor, CblasNoTrans, m, n,
					1.0, A, n, x, 1, 0.0, y, 1);
		diff_sigmoid(theta[i-1], theta[i-1], num_node);
		dot_product(tmp, theta[i-1], num_node);

		cblas_dscal(num_node * num_node, 0.0, delta[i - 1], 1);
		cblas_dger(CblasRowMajor, num_node, num_node, 1.0,
				a[i - 1], 1, tmp, 1, delta[i - 1], num_node);

		//cblas_daxpy(num_node * num_node, LAMBDA, weight[i - 1], 1, delta[i - 1], 1);
		cblas_daxpy(num_node * num_node, STEP_SIZE, delta[i - 1], 1, weight[i - 1], 1);
		cblas_daxpy(num_node, STEP_SIZE, tmp, 1, &weight[i - 1][num_node * num_node], 1);

		//cblas_dgemv(CblasRowMajor, CblasNoTrans, num_node, num_node,
		//		1.0, weight[i - 1], num_node, theta[i], 1, 0.0, tmp, 1);
		m = num_node;
		x = tmp;
		A = weight[i - 1];
	}

	/*in to hidden*/
	cblas_dgemv(CblasRowMajor, CblasNoTrans, m, n,
			1.0, A, n, x, 1, 0.0, y, 1);
	diff_sigmoid(theta[0], theta[0], num_node);
	dot_product(tmp, theta[i], num_node);

	cblas_dscal(num_node * num_feature, 0.0, input_delta, 1);
	cblas_dger(CblasRowMajor, num_feature, num_node, 1.0,
			data, 1, tmp, 1, input_delta, num_node);

	//cblas_daxpy(num_node * num_feature, LAMBDA, input_weight, 1, input_delta, 1);
	cblas_daxpy(num_node * num_feature, STEP_SIZE, input_delta, 1, input_weight, 1);
	cblas_daxpy(num_node, STEP_SIZE, tmp, 1, &input_weight[num_node * num_feature], 1);

	return;
}

void init(double **data, int **result,
		double **input_weight, double **output_weight, double ***weight,
		double **input_delta, double **output_delta, double ***delta,
		double **a_final, double ***a, double ***theta, double **tmp,
		int num_feature, int num_class, int num_layer, int num_node, int num_data, int num_val) {
	int i, j;
	/*(*result) = (int*)malloc(sizeof(int) * num_data);
	(*data) = (double*)malloc(sizeof(double) * num_data * num_feature);*/

	(*input_weight) = (double*)malloc(sizeof(double) * (num_feature + 1) * num_node);
	for(i = 0; i < (num_feature + 1) * num_node; i++) {
		(*input_weight)[i] = ((double)rand()/(double)RAND_MAX) * INIT_RANGE * 2.0 - INIT_RANGE;
	}
	(*output_weight) = (double*)malloc(sizeof(double) * (num_node + 1) * num_class);
	for(i = 0; i < (num_node + 1) * num_class; i++) {
		(*output_weight)[i] = ((double)rand()/(double)RAND_MAX) * INIT_RANGE * 2.0 - INIT_RANGE;
	}
	(*weight) = (double**)malloc(sizeof(double*) * (num_layer - 1));
	for(i = 0; i < (num_layer - 1); i++) {
		(*weight)[i] = (double*)malloc(sizeof(double) * (num_node + 1) * num_node);
		for(j = 0; j < (num_node + 1) * num_node; j++) {
			(*weight)[i][j] = ((double)rand()/(double)RAND_MAX) * INIT_RANGE * 2.0 - INIT_RANGE;
		}
	}

	(*input_delta) = (double*)malloc(sizeof(double) * (num_feature + 1) * num_node);
	for(i = 0; i < (num_feature + 1) * num_node; i++) {
		(*input_delta)[i] = 0.0;
	}
	(*output_delta) = (double*)malloc(sizeof(double) * (num_node + 1) * num_class);
	for(i = 0; i < (num_node + 1) * num_class; i++) {
		(*output_delta)[i] = 0.0;
	}
	(*delta) = (double**)malloc(sizeof(double*) * (num_layer - 1));
	for(i = 0; i < (num_layer - 1); i++) {
		(*delta)[i] = (double*)malloc(sizeof(double) * (num_node + 1) * num_node);
		for(j = 0; j < (num_node + 1) * num_node; j++) {
			(*delta)[i][j] = 0.0;
		}
	}

	(*a_final) = (double*)malloc(sizeof(double) * num_class);
	(*a) = (double**)malloc(sizeof(double*) * num_layer);
	(*theta) = (double**)malloc(sizeof(double*) * num_layer);
	for(i = 0; i < num_layer; i++) {
		(*theta)[i] = (double*)malloc(sizeof(double) * num_node);
		(*a)[i] = (double*)malloc(sizeof(double) * (num_node + 1));
		(*a)[i][num_node] = 1.0;
	}

	(*tmp) = (double*)malloc(sizeof(double) * num_node);

	return;
}

void clean(double *data, int *result,
		double *input_weight, double *output_weight, double **weight,
		double *input_delta, double *output_delta, double **delta,
		double *a_final, double **a, double **theta, double *tmp,
		int num_feature, int num_class, int num_layer, int num_node, int num_data, int num_val) {
	int i;
	free(result);
	free(data);

	free(input_weight);
	free(output_weight);
	for(i = 0; i < (num_layer - 1); i++) {
		free(weight[i]);
	}
	free(weight);

	free(input_delta);
	free(output_delta);
	for(i = 0; i < (num_layer - 1); i++) {
		free(delta[i]);
	}
	free(delta);

	free(a_final);
	for(i = 0; i < num_layer; i++) {
		free(a[i]);
		free(theta[i]);
	}
	free(a);
	free(theta);

	free(tmp);

	return;
}

void shuffle(int *list, int length) {
    int i, j;
    for(i = 0; i < length; i++) {
        list[i] = length;
    }
    for(i = 0; i < length; i++) {
        j = rand() % length;
        while(list[j % length] != length) j++;
        list[j] = i;
    }
    return;
}

int main(int narg, char **args) {
    hid_t file, dset, dspace;
    hsize_t size[4], msize[4];

	int iter1, iter2, i;
    int *y, *test_y, *vali_y;
    double *X, *test_X, *vali_X;
	int num_data, num_feature, num_class, num_test, num_vali, num_training;
	int num_layer, num_node;
	int count_validation, count_validation_top5, found_in_top5;
	double old_rate_val0 = 0.0, old_rate_val1 = 0.0, rate_val;
    int *data_order;

	double *input_weight, *output_weight, **weight;
	double *input_delta, *output_delta, **delta;
	double *a_final, **a, **theta;
	double *tmp;

	int right_count = 0;
    FILE *report;
    char varname[50];

	printf("Program started\n");
    report = fopen(REPORT_FILE, "a");
	fprintf(report, "Program started\n");
    fclose(report);
	srand(time(NULL));

    /*read data*/
    file = H5Fopen("../ann_data/test.h5", H5F_ACC_RDONLY, H5P_DEFAULT);
    dset = H5Dopen(file, "/LABEL", H5P_DEFAULT);
    dspace = H5Dget_space(dset);
    H5Sget_simple_extent_dims(dspace, size, msize);
    test_y = (int*)malloc(sizeof(int) * size[0]);
    H5Dread(dset, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, test_y);
    H5Sclose(dspace);
    H5Dclose(dset);

    dset = H5Dopen(file, "/FEATURE", H5P_DEFAULT);
    dspace = H5Dget_space(dset);
    H5Sget_simple_extent_dims(dspace, size, msize);
    test_X = (double*)malloc(sizeof(double) * size[0] * size[1]);
    H5Dread(dset, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, test_X);
    H5Sclose(dspace);
    H5Dclose(dset);
    H5Fclose(file);
    num_test = size[0];

    file = H5Fopen("../ann_data/validation.h5", H5F_ACC_RDONLY, H5P_DEFAULT);
    dset = H5Dopen(file, "/LABEL", H5P_DEFAULT);
    dspace = H5Dget_space(dset);
    H5Sget_simple_extent_dims(dspace, size, msize);
    vali_y = (int*)malloc(sizeof(int) * size[0]);
    H5Dread(dset, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, vali_y);
    H5Sclose(dspace);
    H5Dclose(dset);

    dset = H5Dopen(file, "/FEATURE", H5P_DEFAULT);
    dspace = H5Dget_space(dset);
    H5Sget_simple_extent_dims(dspace, size, msize);
    vali_X = (double*)malloc(sizeof(double) * size[0] * size[1]);
    H5Dread(dset, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, vali_X);
    H5Sclose(dspace);
    H5Dclose(dset);
    H5Fclose(file);
    num_vali = size[0];

    file = H5Fopen("../ann_data/0.h5", H5F_ACC_RDONLY, H5P_DEFAULT);
    dset = H5Dopen(file, "/LABEL", H5P_DEFAULT);
    dspace = H5Dget_space(dset);
    H5Sget_simple_extent_dims(dspace, size, msize);
    y = (int*)malloc(sizeof(int) * size[0]);
    //H5Dread(dset, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, y);
    H5Sclose(dspace);
    H5Dclose(dset);

    dset = H5Dopen(file, "/FEATURE", H5P_DEFAULT);
    dspace = H5Dget_space(dset);
    H5Sget_simple_extent_dims(dspace, size, msize);
    X = (double*)malloc(sizeof(double) * size[0] * size[1]);
    //H5Dread(dset, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, X);
    H5Sclose(dspace);
    H5Dclose(dset);
    H5Fclose(file);

    num_feature = size[1];
    num_class = 205;
    num_data = size[0];

    /*file = H5Fopen("../../data/numrec.h5", H5F_ACC_RDONLY, H5P_DEFAULT);

    dset = H5Dopen(file, "/num_feature", H5P_DEFAULT);
    H5Dread(dset, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, &num_feature);

    dset = H5Dopen(file, "/num_class", H5P_DEFAULT);
    H5Dread(dset, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, &num_class);

    dset = H5Dopen(file, "/num_data", H5P_DEFAULT);
    H5Dread(dset, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, &num_data);*/

    //num_vali = num_data * 0.1; /*modify here the ratio of validation data*/
    //num_test = num_data * 0.1; /*modify here the ratio of test data*/
    //num_training = num_data - num_vali - num_test;
    data_order = (int*)malloc(sizeof(int) * num_data);
    shuffle(data_order, num_data);
    //for(iter1 = 0; iter1 < num_data; iter1++) printf("%d ", data_order[iter1]);
    //printf("\n");
    /* the list is randomly shuffled, three parts: 1. training, 2. test, 3. validation
     * the boundary is decided by number of each set */

	/*num_layer = atoi(args[1]);
	num_node = atoi(args[2]);*/
    num_layer = 1;
    num_node = 1024;
	printf("Number of hidden layer %d, each %d nodes\n", num_layer, num_node);
	printf("There are %d data, each %d features\n", num_data, num_feature);
    report = fopen(REPORT_FILE, "a");
	fprintf(report, "Number of hidden layer %d, each %d nodes\n", num_layer, num_node);
	fprintf(report, "There are %d data, each %d features\n", num_data, num_feature);
    fclose(report);

    init(&X, &y,
            &input_weight, &output_weight, &weight,
            &input_delta, &output_delta, &delta,
            &a_final, &a, &theta, &tmp,
            num_feature, num_class, num_layer, num_node, num_data, num_vali);

    /*dset = H5Dopen(file, "/y", H5P_DEFAULT);
    H5Dread(dset, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, y);

    dset = H5Dopen(file, "/X", H5P_DEFAULT);
    H5Dread(dset, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, X);*/

	printf("Memory ready\n");
    report = fopen(REPORT_FILE, "a");
	fprintf(report, "Memory ready\n");
    fclose(report);

	for(iter1 = 0; iter1 < size_data * 20; iter1++) {
        sprintf(varname, "../ann_data/%d.h5", iter1 % size_data);
        file = H5Fopen(varname, H5F_ACC_RDONLY, H5P_DEFAULT);
        dset = H5Dopen(file, "/LABEL", H5P_DEFAULT);
        dspace = H5Dget_space(dset);
        H5Sget_simple_extent_dims(dspace, size, msize);
        //y = (int*)malloc(sizeof(int) * size[0]);
        H5Dread(dset, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, y);
        H5Sclose(dspace);
        H5Dclose(dset);

        dset = H5Dopen(file, "/FEATURE", H5P_DEFAULT);
        dspace = H5Dget_space(dset);
        H5Sget_simple_extent_dims(dspace, size, msize);
        //X = (double*)malloc(sizeof(double) * size[0] * size[1]);
        H5Dread(dset, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, X);
        H5Sclose(dspace);
        H5Dclose(dset);
        H5Fclose(file);


		for(iter2 = 0; iter2 < num_data; iter2++) {
			forward_propagate(X + iter2 * num_feature,
					input_weight, output_weight, weight,
					input_delta, output_delta, delta,
					a_final, a, theta,
					num_feature, num_class, num_layer, num_node);
			backward_propagate(X + iter2 * num_feature, y[iter2],
					input_weight, output_weight, weight,
					input_delta, output_delta, delta,
					a_final, a, theta, tmp,
					num_feature, num_class, num_layer, num_node);
		}
		count_validation = 0;
        count_validation_top5 = 0;
        //if(iter1 % 5 == 0) {
		for(iter2 = 0; iter2 < num_test; iter2++) {
			forward_propagate(test_X + (iter2 * num_feature),
					input_weight, output_weight, weight,
					input_delta, output_delta, delta,
					a_final, a, theta,
					num_feature, num_class, num_layer, num_node);
            found_in_top5 = 0;
			if(cblas_idamax(num_class, a_final, 1) == test_y[iter2]) {
				count_validation++;
                found_in_top5 = 1;
			}
            for(i = 0; i < 4; i++) {
                if(found_in_top5) break;
                a_final[cblas_idamax(num_class, a_final, 1)] = 0.0;
                if(cblas_idamax(num_class, a_final, 1) == test_y[iter2]) {
                    found_in_top5 = 1;
			    }
            }
            count_validation_top5 += found_in_top5;
		}
		rate_val = (double)count_validation / (double)num_test;
		printf("%f %f %f %f\n", rate_val, old_rate_val1, old_rate_val0, (double)count_validation_top5 / (double)num_test);
        report = fopen(REPORT_FILE, "a");
		fprintf(report, "%f %f %f %f\n", rate_val, old_rate_val1, old_rate_val0, (double)count_validation_top5 / (double)num_test);
        fclose(report);
		/*if(rate_val <= old_rate_val0) {
			break;
		}
		else if(rate_val > old_rate_val1) {
			old_rate_val0 = old_rate_val1;
			old_rate_val1 = rate_val;
		}
		else {
			old_rate_val0 = rate_val;
		}*/

        if(rate_val <= old_rate_val0) {
            //break;
        }
        else {
            old_rate_val0 = rate_val;
        } //}
	}
	printf("Training ready\n");
    report = fopen(REPORT_FILE, "a");
	fprintf(report, "Training ready\n");
    fclose(report);

    count_validation_top5 = 0;
	for(iter1 = 0; iter1 < num_vali; iter1++) {
		forward_propagate(vali_X + (iter1 * num_feature),
				input_weight, output_weight, weight,
				input_delta, output_delta, delta,
				a_final, a, theta,
				num_feature, num_class, num_layer, num_node);
        found_in_top5 = 0;
		iter2 = cblas_idamax(num_class, a_final, 1);
		if(iter2 == vali_y[iter1]) {
			right_count++;
            found_in_top5 = 1;
		}
        for(i = 0; i < 4; i++) {
            if(found_in_top5) break;
            a_final[cblas_idamax(num_class, a_final, 1)] = 0.0;
            if(cblas_idamax(num_class, a_final, 1) == vali_y[iter1]) {
                found_in_top5 = 1;
            }
        }
        count_validation_top5 += found_in_top5;
	}

	printf("Right rate is %f %f\n", (double)right_count / (double)num_vali, (double)count_validation_top5 / (double)num_vali);
    report = fopen(REPORT_FILE, "a");
	fprintf(report, "Right rate is %f %f\n", (double)right_count / (double)num_vali, (double)count_validation_top5 / (double)num_vali);
    fclose(report);

    report = fopen(REPORT_FILE, "a");
	for(i = 0; i < (num_feature + 1) * num_node; i++) {
		fprintf(report, "%f ", input_weight[i]);
	}
    fprintf(report, "\n");
	for(i = 0; i < (num_node + 1) * num_class; i++) {
		fprintf(report, "%f ", output_weight[i]);
	}
    fprintf(report, "\n");
    fclose(report);

	clean(X, y,
			input_weight, output_weight, weight,
			input_delta, output_delta, delta,
			a_final, a, theta, tmp,
			num_feature, num_class, num_layer, num_node, num_data, num_vali);
    free(test_X);
    free(vali_X);
    free(test_y);
    free(vali_y);
	printf("Finished\n");
    report = fopen(REPORT_FILE, "a");
	fprintf(report, "Finished\n");
    fclose(report);
	return 0;
}
