import numpy
import os
import sys

size_forest = 1
#num_sample_per_dir = 100
categorized_path = '../ranger_data/'
raw_path = '../raw_data/npy/'

tree = dict()
f = open('category', 'r')
while True:
    string = f.readline()
    if len(string) == 0:
        break
    name = string[:string.find(',')]
    string = string[string.find(',') + 1:]
    level1 = string[:string.find(',')]
    string = string[string.find(',') + 1:]
    level2 = string[:-1]
    if level1 not in tree:
        tree[level1] = dict()
    if level2 not in tree[level1]:
        tree[level1][level2] = []
    tree[level1][level2].append(name)

f.close()

#print tree

#for i in os.listdir(categorized_path):
#    os.remove(categorized_path + i)


string = "l"
for k in range(4096):
    string += ' f{}'.format(k)
string += '\n'
for i in range(size_forest):
    #if i < 48:
    #    k = i % 6
    #else:
    #    k = size_forest
    f = open(categorized_path + 'super', 'w')
    f.write(string)
    f.close()

count = 0
for item in os.listdir(raw_path):
    print count
    count += 1
    name = item[:item.find('.')]
    feature = numpy.load(raw_path + item)
    num_sample_per_dir = feature.shape[0] / 51
    num_sample_per_dir = min(num_sample_per_dir, 200)
    #for i in range(size_forest):
    for i in range(size_forest):
        #if i < 48:
        #    k = i % 6
        #else:
        #    k = size_forest
        f = open(categorized_path + 'super', 'a')
        for j in range(1000):
            string = name
            for k in range(feature.shape[1]):
                string += ' {}'.format(feature[i * num_sample_per_dir + j, k])
            string += '\n'
            f.write(string)
        f.close()
    feature = 0

print 'prep done'
