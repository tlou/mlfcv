import os
import sys
import numpy
import socket
import random
import time

data_path = "../ranger_data/"
model_path = data_path + "model/"
ranger_path = "../ranger/"

hostname = socket.gethostname()
random.seed(time.time())

def load_from_pred(name):
    f = open(name, 'r')

    string = f.readline()
    string = f.readline()
    num_class = string.count(' ')
    distribution = []
    label = []
    for i in range(num_class):
        l = int(string[ :string.find(' ')])
        label.append(l)
        string = string[string.find(' ') + 1: ]

    string = f.readline()
    while True:
        line_parsed = []
        string = f.readline()
        if len(string) < 4:
            break
        for i in range(num_class):
            p = float(string[ :string.find(' ')])
            string = string[string.find(' ') + 1: ]
            line_parsed.append(p)

        distribution.append(line_parsed)

    f.close()
    return (label, numpy.array(distribution))

def check_acc(name, ground_truth):
    f = open(name, 'r')
    string = f.readline()
    string = f.readline()
    num_class = string.count(' ')
    label = []
    for i in range(num_class):
        l = int(string[ :string.find(' ')])
        label.append(l)
        string = string[string.find(' ') + 1: ]
    string = f.readline()
    count = 0
    total = 0
    for gt in ground_truth:
        string = f.readline()
        if len(string) < 4:
            break
        best_p = 0.0
        for i in range(num_class):
            p = float(string[ :string.find(' ')])
            string = string[string.find(' ') + 1: ]
            if p > best_p:
                best_p = p
                best_i = i
        if gt == best_i:
            count = count + 1
        total = total + 1
    return float(count) / float(total)

category = dict()
classifiers = []
nodes = ["all"]

f0 = open('category', 'r')
while True:
    string = f0.readline()
    if len(string) < 4:
        break
    name = string[ :string.find(',')]
    string = string[string.find(',') + 1: ]
    l1 = string[ :string.find(',')]
    string = string[string.find(',') + 1: ]
    l2 = string[ :string.find(',')]
    category[name] = (l1, l2)
f0.close()
places = category.keys()
places.sort()

for place in places:
    if category[place][0] not in nodes:
        nodes.append(category[place][0])

for place in places:
    if category[place][1] not in nodes:
        nodes.append(category[place][1])

for i in range(len(nodes)):
    classifiers.append([i])

for place in places:
    (l1, l2) = category[place]
    l1 = nodes.index(l1)
    l2 = nodes.index(l2)
    if l1 not in classifiers[0]:
        classifiers[0].append(l1)
    if l2 not in classifiers[l1]:
        classifiers[l1].append(l2)
    classifiers[l2].append(places.index(place) + len(nodes))
    category[place] = (l1, l2, places.index(place) + len(nodes))

classifiers = [[0]]
for i in range(205):
    classifiers[0].append(i + len(nodes))

print classifiers

def clear_models():
    if not os.path.isdir(model_path):
        os.system("mkdir " + model_path)
    else:
        os.system("rm -rf " + model_path + "*")

def train_all():
    f0 = open(data_path + "super", 'r')
    head = f0.readline()
    f0.close()

    pwd = os.getcwd()
    os.mkdir("../" + hostname)
    os.chdir("../" + hostname)

    if not os.path.isdir(pwd + "/pool/"):
        os.mkdir(pwd + "/pool/")
        count = 0
        for nodei in range(len(classifiers)):
            if not os.path.isdir(data_path + "model/{}/".format(nodei)):
                os.system("mkdir " + data_path + "model/{}/".format(nodei))
            for j in range(1):
                if os.path.isfile(data_path + "model/{}/{}.forest".format(nodei, j)):
                    continue
                print "will do", nodei, j
                f = open(pwd + "/pool/{}".format(count), "w")
                f.write("{} {}\n".format(nodei, j))
                f.close()
                count += 1

    while len(os.listdir(pwd + "/pool/")):
        task = os.listdir(pwd + "/pool/")[random.randint(0, len(os.listdir(pwd + "/pool/")))]
        f = open(pwd + "/pool/" + task, "r")
        string = f.readline()
        f.close()
        if os.path.isfile(pwd + "/pool/" + task):
            os.remove(pwd + "/pool/" + task)
        else:
            continue

        nodei = int(string[ :string.find(" ")])
        j = int(string[string.find(" ") + 1: ])

        print nodei, j

        children = classifiers[nodei][1:]
        this_model = model_path + str(nodei) + "/"
        f1 = open("./training", "w")
        f1.write(head)
        f0 = open(data_path + "super", "r")
        string = f0.readline()
        while True:
            string = f0.readline()
            if len(string) < 4:
                break
            name = string[ :string.find(' ')]
            if category[name][0] in children:
                string = str(category[name][0]) + string[string.find(' '): ]
                f1.write(string)
            elif category[name][1] in children:
                string = str(category[name][1]) + string[string.find(' '): ]
                f1.write(string)
            elif category[name][2] in children:
                string = str(category[name][2]) + string[string.find(' '): ]
                f1.write(string)
        f0.close()
        f1.close()

        os.system(ranger_path + "ranger --file ./training --probability --write --depvarname l --ntree 50 --memmode 1 --savemem")
        sys.exit()
        os.system("cp ranger_out.forest " + this_model + "{}.forest".format(j))
        os.system("rm ranger*")
        os.system("rm training")

        if not os.path.isdir(pwd + "/pool"):
            break
        if len(os.listdir(pwd + "/pool/")) == 0:
            os.rmdir(pwd + "/pool/")
            break

    os.chdir(pwd)
    os.system("rm -rf ../" + hostname)


def predict():
    pwd = os.getcwd()
    os.mkdir("../" + hostname)
    os.chdir("../" + hostname)

    if not os.path.isdir(pwd + "/pool/"):
        #if os.path.isdir(data_path + "results/"):
        #    os.system('rm -rf ' + data_path + "results/")
        #os.system("mkdir " + data_path + "results/")

        os.mkdir(pwd + "/pool/")
        count = 0
        for nodei in range(len(classifiers)):
            if not os.path.isdir(data_path + "results/{}/".format(nodei)):
                os.system("mkdir " + data_path + "results/{}/".format(nodei))
            for j in range(50):
                if os.path.isfile(data_path + "results/{}/{}.prediction".format(nodei, j)):
                    continue
                f = open(pwd + "/pool/{}".format(count), "w")
                f.write("{} {}\n".format(nodei, j))
                f.close()
                count += 1

    f1 = open("./test", "w")
    f0 = open(data_path + str(50), "r")
    string = f0.readline()
    f1.write(string)
    while True:
        string = f0.readline()
        if len(string) < 4:
            break
        name = string[ :string.find(' ')]
        string = str(category[name][2]) + string[string.find(' '): ]
        f1.write(string)
    f0.close()
    f1.close()

    while len(os.listdir(pwd + "/pool/")):
        task = os.listdir(pwd + "/pool/")[random.randint(0, len(os.listdir(pwd + "/pool/")))]
        f = open(pwd + "/pool/" + task, "r")
        string = f.readline()
        f.close()
        if os.path.isfile(pwd + "/pool/" + task):
            os.remove(pwd + "/pool/" + task)
        else:
            continue

        nodei = int(string[ :string.find(" ")])
        j = int(string[string.find(" ") + 1: ])

        print nodei, j

        children = classifiers[nodei][1:]
        this_result = data_path + "results/{}/".format(nodei)
        this_model = model_path + str(nodei) + "/"

        os.system(ranger_path + "ranger --file ./test --predict " + this_model + "{}.forest --depvarname  l --ntree 500".format(j))
        os.system("cp ranger_out.prediction " + this_result + "{}.prediction".format(j))
        os.system("rm ranger*")

        if not os.path.isdir(pwd + "/pool"):
            break
        if len(os.listdir(pwd + "/pool/")) == 0:
            os.rmdir(pwd + "/pool/")
            break

    os.chdir(pwd)
    os.system("rm -rf ../" + hostname)

def average_match(filename):
    num_samples = 36608
    result_path = data_path + "results/"
    distribution = numpy.zeros([num_samples, 212])
    for nodei in range(7):
        for j in range(50):
            label, this_distribution = load_from_pred(result_path + "{}/{}.prediction".format(nodei, j))
            distribution[:, label] += this_distribution

    distribution /= 50.0
    distribution[:, 0] = 1.0

    for classifier in classifiers:
        father = classifier[0]
        children = classifier[1:]
        for kid in children:
            distribution[:, kid] = distribution[:, kid] * distribution[:, father]

    #for i in range(num_samples):
    #    for classifier in classifiers:
    #        father = classifier[0]
    #        children = classifier[1:]
    #        best_quality = 0.0
    #        for kid in children:
    #            if best_quality < distribution[i, kid]:
    #                best_quality = distribution[i, kid]
    #                the_chosen_one = kid
    #        for kid in children:
    #            if kid == the_chosen_one:
    #                distribution[i, kid] *= distribution[i, father]
    #            else:
    #                distribution[i, kid] = 0.0


    level1_dist = distribution[:, [1, 2]]
    level2_dist = distribution[:, [3, 4, 5, 6]]
    distribution = distribution[:, len(classifiers):]

    matches = numpy.zeros([num_samples, 5], dtype = numpy.int)
    for j in range(num_samples):
        d = distribution[j]
        for i in range(5):
            matches[j, i] = d.argmax()
            d[matches[j, i]] = 0.0

    count_level1 = 0
    count_level2 = 0
    count1 = 0
    count5 = 0
    total = 0
    f = open(filename, "r")
    string = f.readline()
    while True:
        string = f.readline()
        if len(string) < 4:
            break
        name = string[ :string.find(" ")]
        index = category[name][2] - len(classifiers)
        if index == matches[total, 0]:
            count1 += 1
        if index in matches[total]:
            count5 += 1
        i1 = level1_dist[total].argmax() + 1
        i2 = level2_dist[total].argmax() + 3
        if i1 == category[name][0]:
            count_level1 += 1
        if i2 == category[name][1]:
            count_level2 += 1
        total += 1
    f.close()

    print count_level1, count_level2
    print count1, count5, total
    count1 = float(count1) / float(total)
    count5 = float(count5) / float(total)
    count_level1 = float(count_level1) / float(total)
    count_level2 = float(count_level2) / float(total)
    print count_level1, count_level2, count1, count5
    return (count1, count5)

def match_average(filename):
    result_path = data_path + "results/"
    average_distribution = numpy.zeros([20500, 205])
    for j in range(50):
        distribution = numpy.zeros([20500, 212])
        for nodei in range(7):
            label, this_distribution = load_from_pred(result_path + "{}/{}.prediction".format(nodei, j))
            distribution[:, label] += this_distribution

        distribution[:, 0] = 1.0

        for classifier in classifiers:
            father = classifier[0]
            children = classifier[1:]
            for kid in children:
                distribution[:, kid] = distribution[:, kid] * distribution[:, father]
        distribution = distribution[:, len(classifiers):]
        average_distribution += distribution

    average_distribution /= 50.0

    matches = numpy.zeros([20500, 5], dtype = numpy.int)
    for j in range(20500):
        d = distribution[j]
        for i in range(5):
            matches[j, i] = d.argmax()
            d[matches[j, i]] = 0.0
    count1 = 0
    count5 = 0
    total = 0
    f = open(filename, "r")
    string = f.readline()
    while True:
        string = f.readline()
        if len(string) < 4:
            break
        name = string[ :string.find(" ")]
        index = category[name][2] - len(classifiers)
        if index == matches[total, 0]:
            count1 += 1
        if index in matches[total]:
            count5 += 1
        total += 1
    f.close()

    print count1, count5, total
    count1 = float(count1) / float(total)
    count5 = float(count5) / float(total)
    print count1, count5
    return (count1, count5)



if __name__ == "__main__":
    #clear_models()
    train_all()
    #predict()
    #average_match(data_path + "50")
    #match_average(data_path + "50")



