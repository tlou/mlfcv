import os
import socket
import random

model_path = '../models_flat/'
data_path = '../categorized_data/'
libsvm_path = '../libsvm/'
size_forest = 50

#for i in os.listdir(model_path):
#    os.system('rm -rf ' + model_path + i)

place_index = dict()
f = open('./places_list')
while True:
    string = f.readline()
    if len(string) < 4:
        break
    name = string[ :string.find(' ')]
    i = int(string[string.find(' ') + 1: ])
    place_index[name] = i
f.close()
#print place_index

hostname = socket.gethostname()
pwd = os.getcwd()

if not os.path.isdir('./pool/'):
    os.mkdir('./pool')
    for i in range(50):
        f = open('./pool/{}'.format(i), 'w')
        f.write(str(i))
        f.close()

os.mkdir('../' + hostname)
os.chdir('../' + hostname)

for i in [47]:
#while True:
    #if not os.path.isdir(pwd + '/pool/'):
    #    break
    #items = os.listdir(pwd + '/pool/')
    #if len(items) == 0:
    #    os.rmdir(pwd + '/pool/')
    #    break

    #i = items[random.randint(0, len(items))]
    #os.system('rm ' + pwd + '/pool/' + str(i))
    print i
    if not os.path.isdir(model_path + '00'):
        os.system('mkdir ' + model_path + '00')
    f0 = open(data_path + str(i), 'r')
    f = open('./tmp_flat', 'w')
    while True:
        string = f0.readline()
        if len(string) < 5:
            break
        name = string[:string.find(' ')]
        string = str(place_index[name]) + string[string.find(' '):]
        f.write(string)
    f.close()
    f0.close()
    os.system(libsvm_path + 'svm-train tmp_flat')
    os.system('mv tmp_flat.model ' + model_path + '00/{}.model'.format(i))

    os.system('rm tmp_flat')

os.chdir(pwd)
os.system("rm -rf ../" + hostname)
