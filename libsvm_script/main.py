import os
import sys
import numpy

data_path = "../ranger_data/"
model_path = data_path + "model/"
ranger_path = "../ranger/"

def load_from_pred(name):
    f = open(name, 'r')

    string = f.readline()
    string = f.readline()
    num_class = string.count(' ')
    distribution = []
    label = []
    for i in range(num_class):
        l = int(string[ :string.find(' ')])
        label.append(l)
        string = string[string.find(' ') + 1: ]

    string = f.readline()
    while True:
        line_parsed = []
        string = f.readline()
        if len(string) < 4:
            break
        for i in range(num_class):
            p = float(string[ :string.find(' ')])
            string = string[string.find(' ') + 1: ]
            line_parsed.append(p)

        distribution.append(line_parsed)

    f.close()
    return (label, numpy.array(distribution))

def check_acc(name, ground_truth):
    f = open(name, 'r')
    string = f.readline()
    string = f.readline()
    num_class = string.count(' ')
    label = []
    for i in range(num_class):
        l = int(string[ :string.find(' ')])
        label.append(l)
        string = string[string.find(' ') + 1: ]
    string = f.readline()
    count = 0
    total = 0
    for gt in ground_truth:
        string = f.readline()
        if len(string) < 4:
            break
        best_p = 0.0
        for i in range(num_class):
            p = float(string[ :string.find(' ')])
            string = string[string.find(' ') + 1: ]
            if p > best_p:
                best_p = p
                best_i = i
        if gt == best_i:
            count = count + 1
        total = total + 1
    return float(count) / float(total)

category = dict()
classifiers = []
nodes = ["all"]

f0 = open('../ranger_scripts/category', 'r')
while True:
    string = f0.readline()
    if len(string) < 4:
        break
    name = string[ :string.find(',')]
    string = string[string.find(',') + 1: ]
    l1 = string[ :string.find(',')]
    string = string[string.find(',') + 1: ]
    l2 = string[ :string.find(',')]
    category[name] = (l1, l2)
f0.close()
places = category.keys()
places.sort()

for place in places:
    if category[place][0] not in nodes:
        nodes.append(category[place][0])

for place in places:
    if category[place][1] not in nodes:
        nodes.append(category[place][1])

for i in range(len(nodes)):
    classifiers.append([i])

for place in places:
    (l1, l2) = category[place]
    l1 = nodes.index(l1)
    l2 = nodes.index(l2)
    if l1 not in classifiers[0]:
        classifiers[0].append(l1)
    if l2 not in classifiers[l1]:
        classifiers[l1].append(l2)
    classifiers[l2].append(places.index(place) + len(nodes))
    category[place] = (l1, l2, places.index(place) + len(nodes))

for i in places:
    print i, category[i]

def clear_models():
    if not os.path.isdir(model_path):
        os.system("mkdir " + model_path)
    else:
        os.system("rm -rf " + model_path + "*")

def train_all():
    f0 = open(data_path + str(0), 'r')
    head = f0.readline()
    f0.close()

    for nodei in range(len(classifiers)):
        print "now training node {}".format(nodei)
        print "from {} to {}".format(classifiers[nodei][0], classifiers[nodei][1:])
        children = classifiers[nodei][1:]
        this_model = model_path + str(nodei) + "/"
        if os.path.isdir(this_model):
            os.system("rm -rf " + this_model)
        os.system("mkdir " + this_model)
        for j in range(50):
            f1 = open("./training", "w")
            f1.write(head)
            f0 = open(data_path + str(j), "r")
            string = f0.readline()
            while True:
                string = f0.readline()
                if len(string) < 4:
                    break
                name = string[ :string.find(' ')]
                if category[name][0] in children:
                    string = str(category[name][0]) + string[string.find(' '): ]
                    f1.write(string)
                elif category[name][1] in children:
                    string = str(category[name][1]) + string[string.find(' '): ]
                    f1.write(string)
                elif category[name][2] in children:
                    string = str(category[name][2]) + string[string.find(' '): ]
                    f1.write(string)
            f0.close()
            f1.close()

            os.system(ranger_path + "ranger --file ./training --probability --write --depvarname l --ntree 500")
            os.system("cp ranger_out.forest " + this_model + "{}.forest".format(j))
            os.system("rm ranger*")
            os.system("rm training")

def predict():
    #f0 = open(data_path + str(0), 'r')
    #head = f0.readline()
    #f0.close()
    if not os.path.isdir(data_path + "results/"):
        os.system("mkdir " + data_path + "results/")

    #f1 = open("./test", "w")
    #f1.write(head)
    #f0 = open(data_path + str(50), "r")
    #string = f0.readline()
    #while True:
    #    string = f0.readline()
    #    if len(string) < 4:
    #        break
    #    name = string[ :string.find(' ')]
    #    string = str(category[name][2]) + string[string.find(' '): ]
    #    f1.write(string)
    #f0.close()
    #f1.close()

    for nodei in [5,6]:#range(len(classifiers)):
        print "now predicting on node {}".format(nodei)
        children = classifiers[nodei][1:]
        this_result = data_path + "results/{}/".format(nodei)
        this_model = model_path + str(nodei) + "/"
        if os.path.isdir(this_result):
            os.system("rm -rf " + this_result)
        os.system("mkdir " + this_result)

        for j in range(50):
            os.system(ranger_path + "ranger --file ../ranger_scripts/test --predict " + this_model + "{}.forest --depvarname  l --ntree 500".format(j))
            os.system("cp ranger_out.prediction " + this_result + "{}.prediction".format(j))
            os.system("rm ranger*")

    #os.system("rm test")

def compile_result():
    result_path = data_path + "result/"
    for nodei in range(len(classifiers)):
        this_result = result_path + str(nodei) + "/"
        label, distribution = load_from_pred(this_result + "{}.prediction".format(0))
        for j in range(1, 50):
            label, this_distribution = load_from_pred(this_result + "{}.prediction".format(j))
            distribution += this_distribution

        this_distribution /= 50.0






if __name__ == "__main__":
    #clear_models()
    #train_all()
    predict()
    pass
